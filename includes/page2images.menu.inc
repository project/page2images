<?php
/**
 * @file
 * Menu callback functions.
 */

/**
 * Menu callback to handle notifications originated from Page2Images service
 * provider.
 *
 * @param string $entity_type
 *  The type of the entity, e.g. 'node', 'user'... etc.
 *
 * @param string $field
 *  The name of the specified field.
 *
 * @param string $token
 *  The generated MD5 hash token for the specified field item.
 */
function page2images_notification_listener($entity_type = '', $field = '', $token = '') {
  $result = isset($_POST['result']) ? $_POST['result'] : FALSE;

  if (!empty($entity_type) && !empty($field) && !empty($token) && $result) {
    $result = rawurldecode($result);
    $json_data = drupal_json_decode($result);

    if (!isset($json_data['status'])) {
      drupal_exit();
    }

    switch ($json_data['status']) {
      case 'error':
        watchdog('P2I', $json_data['errno'] . ' ' . $json_data['msg']);
        break;

      case 'finished':
        $details = array(
          // URL to generated image.
          'source_url' => check_plain($json_data['image_url']),
          // E.g. 'node'.
          'entity_type' => check_plain($entity_type),
          // E.g. 'field_page2images'
          'field_name' => check_plain($field),
          // MD5 hash code.
          'token' => check_plain($token),
        );

        page2images_attach_file_to_entity($details);
        break;
    }
  }
  else {
    // It's empty. Do nothing.
  }
}
