<?php
/**
 * @file
 * Administrative page callbacks for the page2images module.
 */

/**
 * Implements hook_form().
 */
function page2images_settings_form($form, &$form_state) {

  $form['page2images_rest_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Rest API key'),
    '#description' => t('You can get your Rest API key by signing up at: !link', array('!link' => l('Page2Images', 'http://www.page2images.com'))),
    '#default_value' => variable_get('page2images_rest_api_key', ''),
    '#required' => FALSE,
  );

//  $form['page2images_direct_linking_api_key'] = array(
//    '#type' => 'textfield',
//    '#title' => t('Direct Linking API key'),
//    '#description' => t('You can get your Rest API key by signing up at: !link', array('!link' => l('Page2Images', 'http://www.page2images.com'))),
//    '#default_value' => variable_get('page2images_direct_linking_api_key', ''),
//    '#required' => FALSE,
//  );

  return system_settings_form($form);
}
