<?php

/**
 * @file
 * Hooks provided by the page2images module.
 */
/**
 * @addtogroup hooks
 * @{
 */

/**
 * This hook allows to alter query parameters before sending the request to
 * Page2Images.
 *
 * @param array $query_data
 *   The query array which contains parameters for http request.
 */
function hook_page2images_query_array_alter(&$query_data) {

}

/**
 * @} End of "addtogroup hooks".
 */
